$(document).ready(function() {

  $(window).load(function() {
    $("#content-loader").delay(1000).fadeOut(1000);
  });

  $(".part-link.part0").click(function() {
    $("#part-menu").slideUp(500);
    $("#part1, #part2").delay(800).fadeOut(1000);
    $("#part0").delay(800).fadeIn(1000);
  });

  $("#part0 .part-link.part1").hover(
    function() {
      $("#part0 .part-bg.part1").stop().fadeIn(1000);
    },
    function() {
      $("#part0 .part-bg.part1").stop().fadeOut(1000);
    });

  $("#part0 .part-link.part2").hover(
    function() {
      $("#part0 .part-bg.part2").stop().fadeIn(1000);
    },
    function() {
      $("#part0 .part-bg.part2").stop().fadeOut(1000);
    });

  $(".part-link.part1").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part2").delay(800).fadeOut(1000);
    $("#part1").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".part-link.part2").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);
    $("#part-menu").slideUp(500);
    $("#part0, #part1").delay(800).fadeOut(1000);
    $("#part2").delay(800).fadeIn(1000);
    $("#part-menu").delay(1500).slideDown(500);
  });

  $(".fancybox").fancybox({
    closeBtn  : false,
    arrows    : true,
    nextClick : false,
    mouseWheel: true,

    tpl: {
      error: '<p class="fancybox-error">Невозможно загрузить изображение.<br/>Повторите попытку позже.<br/>Если ошибка будет повторяться,<br/>вы можете написать на <a href="mailto:zvorygin@yararchive.ru?subject=Проблемы с показом изображений в электронной выставке Страницы мужества и героизма. Ярославль в Великой Отечественной войне 1941-1945 гг.">zvorygin@yararchive.ru</a></p>'
    },

    helpers : {
      thumbs : {
        width  : 50,
        height : 50
      }
    }
  });

  $("#img1_1-colored").hover(
    function() {
      $(this).attr("src", "img/docs/thumbs/1_1_color.jpg");
    },
    function() {
      $(this).attr("src", "img/docs/thumbs/1_1.jpg");
    });
});